package main

import (
	"flag"
	"fmt"
	"github.com/cheggaaa/pb/v3"
	"gitlab.com/iccfpga-rv/iccfpga-go-lib/iccfpga"
	"os"
)

var endpoint = "https://devnet.nodes.thetangle.org:443"

// has to be kept secret ... the weakness of everything :scream:
var apiKey = [...]byte{0xb2, 0x33, 0x12, 0x56, 0x41, 0xf0, 0xcc, 0x60, 0x9c, 0x6f, 0x36, 0x30, 0xe7, 0xf3, 0xb2, 0xfd, 0xb7, 0x2b, 0xbd, 0x2e,
	0x94, 0x40, 0xa5, 0xb0, 0x0a, 0xb5, 0x83, 0x65, 0x5b, 0x01, 0xb1, 0x43, 0xdf, 0x31, 0x3e, 0x9a, 0xa0, 0x90, 0x72, 0x02, 0xdf, 0x5f, 0x16,
	0x40, 0x8e, 0x64, 0xf4, 0xd4}

var bar *pb.ProgressBar

func progress(start int, end int, progress int) {
	if bar == nil {
		bar = pb.StartNew(end - start)
	}
	bar.Increment()
}

func main() {
	filename := flag.String("file", "design_iccfpga_wrapper.bin", "Binary ICCFPGA Core File")
	cmd := flag.String("cmd", "", "Read/Write from/to QSPI flash")
	device := flag.String("device", "/dev/ttyS0", "Output file - default serial")
	pageOfs := flag.Uint64("page", uint64(0), "Start page to read from / to write to")
	debug := flag.Bool("debug", false, "Debug output")

	flag.Parse()

	// assume fixed length
	//	numPages := flag.Uint64("num-pages", 0, "Number of bytes to read / write")

	fmt.Println(*cmd)
	if *cmd != "read" && *cmd != "write" {
		fmt.Printf("usage:\n")
		flag.PrintDefaults()
		os.Exit(1)
	}

	config := iccfpga.ICCFPGAConfig{
		Serial:        *device,
		ApiKey:        apiKey,
		KeepSeedInRAM: true,
		Verbose:       false,
		Debug:         *debug,
	}
	var iccfpga iccfpga.ICCFPGA

	err := iccfpga.Init(config)
	must(err)

	version, err := iccfpga.GetVersion()
	must(err)

	fmt.Printf("version: %s\n", version)

	if *cmd == "read" {
		err := iccfpga.ReadFlashToFile(*filename, int(*pageOfs), progress)
		must(err)
	} else if *cmd == "write" {
		err := iccfpga.WriteFlashFromFile(*filename, uint32(*pageOfs), progress)
		must(err)
	}
	if bar != nil {
		bar.Finish()
	}
	os.Exit(0)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
