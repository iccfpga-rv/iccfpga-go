module gitlab.com/iccfpga-rv/iccfpga-go

go 1.13

require (
	github.com/cheggaaa/pb/v3 v3.0.4
	github.com/fatih/color v1.9.0 // indirect
	github.com/fatih/structs v1.1.0
	github.com/iotaledger/iota.go v1.0.0-beta.14
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/iccfpga-rv/iccfpga-go-lib v0.0.0-20200309153651-72a4acaeb5b7
	gitlab.com/iccfpga/iota.go-test v0.0.0-20190315160158-d2e0920f4d98
	golang.org/x/sys v0.0.0-20200212091648-12a6c2dcc1e4 // indirect
)

//replace gitlab.com/iccfpga-rv/iccfpga-go-lib => ../iccfpga-go-lib
