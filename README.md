IOTA Crypto Core 
================

You will need the latest Golang (1.13+).
```bash
# Make sure to download the correct package for your OS. Raspberry is another one, for example.
wget https://dl.google.com/go/go1.13.8.linux-amd64.tar.gz
# Unpack and move to the correct location:
tar -xvf  go1.13.8.linux-amd64.tar.gz
sudo mv go /usr/local
```
UBUNTU default Go package: Be aware that Ubuntu usually comes with an outdated version. You can check what version your go is with go version command.

If you want to run the Go-programs on the Raspberry Pi, you need

`https://dl.google.com/go/go1.13.8.linux-armv6l.tar.gz`
instead of amd64-version from above!

You need to specify GOROOT, GOPATH and adjust PATH to include the new go directory.

Simply add these to your .profile:
```bash
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
```
Now run go version to make sure that everything works and you have the correct version installed. You may need to reboot (sudo reboot) for the go env variable changes to take effect.


Get repository and run the test program
------------------------------------
After you installed go, created the go-Path and set the environment variables, you can now download and test example programs
```bash
git clone https://gitlab.com/iccfpga-rv/iccfpga-go
cd iccfpga-go/dummy-value-example
go build
```

Now, there should be an binary file called dummy-value-example.

There are a coupld of arguments the program supports:
```bash
$ ./dummy-value-example -h

Usage of ./dummy-value-example:
  -count int
        Number of bundles to generate (default 10)
  -debug
        Debug output
  -device string
        serial device (default "/dev/ttyUSB0")
  -endpoint string
        IOTA node to use (default "https://mainnet01.hornet.zone")
  -mwm int
        Min-Weight-Magnitude; Use 9 for devnet, 14 for mainnet (default 14)
```

When connecting the ICCFPGA-dev board via USB to the PC, the device is likely to become `/dev/ttyUSB0`. 

If using the dev-board on the Raspberry Pi, you will have to use `/dev/ttyS0` as serial device.

When starting the test-program - in this case the dev-Board is connected to USB of a PC - following output is expected:
```bash
$ ./dummy-value-example -device /dev/ttyUSB0 -endpoint https://nodes.devnet.thetangle.org -mwm 9
iccfpga version: 0.14rv

generating random seed ... generated

generating 10 addresses ... done

broadcasted bundle with tail tx hash:  TPVZXSUMOITBWPEUZVRAXGUORSI9CBRISWSYWCAE9UMRL9ARAGSOVRVCZXNZAEZVLBJKLIFEZIVZQE999
broadcasted bundle with tail tx hash:  KYCHWJHSOAMS9KBI9JGVZR99PYEIEVQRYHUXVMAWNAZHQBNOXZNRPCHSPWZXUCPMKUCUPLRJDEDXXW999
broadcasted bundle with tail tx hash:  WKINAAAAPJNGXPAGMPHBUASMHAJHREADIXPLYZWWBOGE99CMUDD9UINJHNUUXPUAGEHANXOCLG9VML999
broadcasted bundle with tail tx hash:  OCTNQRGSUMU9FDVSCDHPMZTGCHANXFWUKUMM9ISJBMLYDYXVGRUDJTMVTAUVYNNLFKNIMBN9ICUQYL999
broadcasted bundle with tail tx hash:  HLZEMYDDNQXJAXUYS9IALZODXGGPVPVOECOBNWMAJXERSWTOMUAF9YBGPVMAFCKPIERIQXRWPVRJVI999
broadcasted bundle with tail tx hash:  SSRP9PXBVJAIASTRQLCQLHCPBHAAEGJSVC9TDLGLSBCIAAY9GSDSSBVFGDIKOTUKMH9YLSZKUHJZEK999
broadcasted bundle with tail tx hash:  YL9YCKCSMDXHYFIILYIQIWFO9RKBSXTVAGFTRLHEFDEUAOYXLNSWNLGOSUSJGGLTUJWV9MNUNYUJYR999
broadcasted bundle with tail tx hash:  HKLKXZUPUFLSLEWSGBOOQSOSVGEVNXZTELMQQADXJDQSSIFXWJPDACONZVUGMOYBSD9AKKPEYYXMLO999
broadcasted bundle with tail tx hash:  IUMASWJIPSTIJTAJEKPN9BEFXGAUIZNWTFAKUBAFPOBCYOJBDHLROV99XIXARFAIKATGZQHOPNWJZK999
broadcasted bundle with tail tx hash:  XQTIGYIV9MMTDLNRIVWWTADKAFIXLWOWSVARQXTERJXQKGRCSEVAF9BXJPTGGKVMVEU9BZBDRDDTJU999
```

What does the test-program do?
------------------------------

It shows how to use golang for

- initializing the IOTA Crypto Core
- generating a new seed
- generating new addresses based on the seed
- issue signed value transaction with PoW

The test-program does dummy-value transfers (input and output address the same) for that no real coins are needed.

One confirmed bundle from the test above:
https://devnet.thetangle.org/transaction/HLZEMYDDNQXJAXUYS9IALZODXGGPVPVOECOBNWMAJXERSWTOMUAF9YBGPVMAFCKPIERIQXRWPVRJVI999