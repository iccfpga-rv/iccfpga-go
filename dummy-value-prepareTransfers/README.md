dummy value example using prepareTransfers
==========================================

This example sends 1ki from one address to the same address. It's only a dummy transfer that doesn't need real IOTAs.

The example shows:
- how to generate a random seed
- how to generate addresses 
- how to use prepareTransfers for creating (PoWed) bundles on the Crypto Core
- how to use the crypto-core with iota.go


