dummy value example
===================

This example sends 1ki from one address to the same address. It's only a dummy transfer that doesn't need real IOTAs.

The example shows:
- how to generate a random seed
- how to generate addresses 
- how to sign transactions
- how to use the crypto-core with iota.go


