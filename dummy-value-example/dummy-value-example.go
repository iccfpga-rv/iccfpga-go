package main

import (
	"flag"
	"fmt"

	. "github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/bundle"
	. "github.com/iotaledger/iota.go/consts"
	"gitlab.com/iccfpga-rv/iccfpga-go-lib/iccfpga"
)

// how many milestones back to start the random walk from
const depth = 3

var keyIndex int = 0

// has to be kept secret ... the weakness of everything :scream:
var apiKey = [...]byte{0xb2, 0x33, 0x12, 0x56, 0x41, 0xf0, 0xcc, 0x60, 0x9c, 0x6f, 0x36, 0x30, 0xe7, 0xf3, 0xb2, 0xfd, 0xb7, 0x2b, 0xbd, 0x2e,
	0x94, 0x40, 0xa5, 0xb0, 0x0a, 0xb5, 0x83, 0x65, 0x5b, 0x01, 0xb1, 0x43, 0xdf, 0x31, 0x3e, 0x9a, 0xa0, 0x90, 0x72, 0x02, 0xdf, 0x5f, 0x16,
	0x40, 0x8e, 0x64, 0xf4, 0xd4}

func main() {
	//	device := flag.String("device", "/dev/ttyS0", "serial device")
	device := flag.String("device", "/dev/ttyUSB0", "serial device")
	endpoint := flag.String("endpoint", "https://mainnet01.hornet.zone", "IOTA node to use")
	mwm := flag.Int("mwm", 14, "Min-Weight-Magnitude; Use 9 for devnet, 14 for mainnet")
	numTX := flag.Int("count", 10, "Number of bundles to generate")
	debug := flag.Bool("debug", false, "Debug output")

	flag.Parse()

	if *numTX > 100 {
		panic("too many bundles")
	}

	if *mwm != 9 && *mwm != 14 {
		panic("use mwm 9 or 14")
	}

	config := iccfpga.ICCFPGAConfig{
		Serial:        *device,
		ApiKey:        apiKey,
		KeepSeedInRAM: true,
		Verbose:       false,
		Debug:         *debug,
	}
	var iccfpga iccfpga.ICCFPGA

	err := iccfpga.Init(config)
	must(err)

	version, err := iccfpga.GetVersion()
	must(err)

	fmt.Printf("iccfpga version: %s\n\n", version)

	// create a new API instance
	api, err := ComposeAPI(HTTPClientSettings{
		URI: *endpoint,
		// (!) if no PoWFunc is supplied, then the connected node is requested to do PoW for us
		// via the AttachToTangle() API call.
		LocalProofOfWorkFunc: iccfpga.PoWFunc,
	})
	must(err)

	fmt.Printf("generating random seed ... ")
	err = iccfpga.GenerateRandomSeed(0)
	must(err)
	fmt.Printf("generated\n\n")

	// used here only for getting an receiving address
	fmt.Printf("generating %d addresses ... ", *numTX)
	addresses, err := iccfpga.GenerateAddresses("", uint64(0), uint64(*numTX), SecurityLevelMedium, true)
	must(err)
	fmt.Printf("done\n\n")

	for key := uint64(0); key < uint64(*numTX); key++ {
		// create a transfer to the given recipient address
		// optionally define a message and tag
		transfers := bundle.Transfers{
			{
				Address: addresses[key],
				Value:   1000,
				Tag:     "ICCFPGA9TEST",
			},
		}

		// create inputs for the transfer
		inputs := []Input{
			{
				Address:  addresses[key],
				Security: SecurityLevelMedium,
				KeyIndex: key,
				Balance:  1000,
			},
		}

		// we don't need to set the security level or timestamp in the options because in this
		// example input and output is the same without remainder
		prepTransferOpts := PrepareTransfersOptions{Inputs: inputs}
		//	prepTransferOpts := PrepareTransfersOptions{Inputs: inputs, RemainderAddress: &remainderAddress}

		// prepare the transfer by creating a bundle with the given transfers and inputs.
		// the result are trytes ready for PoW.
		trytes, err := PrepareTransfers(&iccfpga, keyIndex, transfers, prepTransferOpts)
		must(err)

		// at this point the bundle trytes are signed.
		// now we need to:
		// 1. select two tips
		// 2. do proof-of-work
		// 3. broadcast the bundle
		// 4. store the bundle
		// SendTrytes() conveniently does the steps above for us.
		bndl, err := api.SendTrytes(trytes, depth, uint64(*mwm))
		must(err)

		fmt.Println("broadcasted bundle with tail tx hash: ", bundle.TailTransactionHash(bndl))
	}
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
