package main

import (
	"strings"
	"time"

	"github.com/iotaledger/iota.go/api"
	"github.com/iotaledger/iota.go/bundle"
	. "github.com/iotaledger/iota.go/consts"
	. "github.com/iotaledger/iota.go/guards/validators"
	"github.com/iotaledger/iota.go/transaction"
	. "github.com/iotaledger/iota.go/trinary"
	"gitlab.com/iccfpga-rv/iccfpga-go-lib/iccfpga"
)

var Api *api.API

// only a dummy seed because the real seed is on the crypto-core
var emptySeed = strings.Repeat("9", 81)

// PrepareTransfers prepares the transaction trytes by generating a bundle, filling in transfers and inputs,
// adding remainder and signing all input transactions.
func PrepareTransfers(iccfpga *iccfpga.ICCFPGA, slot int, transfers bundle.Transfers, opts api.PrepareTransfersOptions) ([]Trytes, error) {
	//seed := emptySeed
	//	opts = getPrepareTransfersDefaultOptions(opts)
	if opts.Security == 0 {
		opts.Security = SecurityLevelMedium
	}
	if opts.Inputs == nil {
		opts.Inputs = []api.Input{}
	}
	/*
		if err := Validate(ValidateSeed(seed), ValidateSecurityLevel(opts.Security)); err != nil {
			return nil, err
		}
	*/
	for i := range transfers {
		if err := Validate(ValidateAddresses(transfers[i].Value != 0, transfers[i].Address)); err != nil {
			return nil, err
		}
	}

	var timestamp uint64
	txs := transaction.Transactions{}

	if opts.Timestamp != nil {
		timestamp = *opts.Timestamp
	} else {
		timestamp = uint64(time.Now().UnixNano() / int64(time.Second))
	}

	var totalOutput uint64
	for i := range transfers {
		totalOutput += transfers[i].Value
	}

	// add transfers
	outEntries, err := bundle.TransfersToBundleEntries(timestamp, transfers...)
	if err != nil {
		return nil, err
	}
	for i := range outEntries {
		txs = bundle.AddEntry(txs, outEntries[i])
	}
	/*
		// gather inputs if we have api value transfer but no inputs were given.
		// this would error out if the gathered inputs don't fulfill the threshold value
		if totalOutput != 0 && len(opts.Inputs) == 0 {
			inputs, err := Api.GetInputs(seed, api.GetInputsOptions{Security: opts.Security, Threshold: &totalOutput})
			if err != nil {
				return nil, err
			}

			// filter out inputs which are already spent
			inputAddresses := make(Hashes, len(opts.Inputs))
			for i := range opts.Inputs {
				inputAddresses[i] = inputs.Inputs[i].Address
			}

			states, err := Api.WereAddressesSpentFrom(inputAddresses...)
			if err != nil {
				return nil, err
			}
			for i, state := range states {
				if state {
					inputs.Inputs = append(inputs.Inputs[:i], inputs.Inputs[i+1:]...)
				}
			}

			opts.Inputs = inputs.Inputs
		}
	*/
	// add input transactions
	var totalInput uint64
	for i := range opts.Inputs {
		if err := Validate(ValidateAddresses(opts.Inputs[i].Balance != 0, opts.Inputs[i].Address)); err != nil {
			return nil, err
		}
		totalInput += opts.Inputs[i].Balance
		input := &opts.Inputs[i]
		bndlEntry := bundle.BundleEntry{
			Address:   input.Address[:HashTrytesSize],
			Value:     -int64(input.Balance),
			Length:    uint64(input.Security),
			Timestamp: timestamp,
		}
		txs = bundle.AddEntry(txs, bndlEntry)
	}

	// verify whether provided inputs fulfill threshold value
	if totalInput < totalOutput {
		return nil, ErrInsufficientBalance
	}

	/*
		// compute remainder
		remainder := totalInput - totalOutput
		// add remainder transaction if there's a remainder
		if remainder > 0 {
			// compute new remainder address if non supplied
			if opts.RemainderAddress == nil {
				remainderAddressKeyIndex := opts.Inputs[0].KeyIndex
				for i := range opts.Inputs {
					keyIndex := opts.Inputs[i].KeyIndex
					if keyIndex > remainderAddressKeyIndex {
						remainderAddressKeyIndex = keyIndex
					}
				}
				remainderAddressKeyIndex++
				addrs, err := Api.GetNewAddress(seed, api.GetNewAddressOptions{Security: opts.Security, Index: remainderAddressKeyIndex})
				if err != nil {
					return nil, err
				}
				opts.RemainderAddress = &addrs[0]
			} else {
				if err := Validate(ValidateAddresses(true, *opts.RemainderAddress)); err != nil {
					return nil, ErrInvalidRemainderAddress
				}
				// make sure to remove checksum from remainder address
				cleanedAddr, err := checksum.RemoveChecksum(*opts.RemainderAddress)
				if err != nil {
					return nil, err
				}
				opts.RemainderAddress = &cleanedAddr
			}

			// add remainder transaction
			txs = bundle.AddEntry(txs, bundle.BundleEntry{
				Address: (*opts.RemainderAddress)[:HashTrytesSize],
				Length:  1, Timestamp: timestamp,
				Value: int64(remainder),
			})
		}
	*/
	/* disabled for testing
	// verify that input txs don't send to the same address
	for i := range txs {
		tx := &txs[i]
		// only check output txs
		if tx.Value <= 0 {
			continue
		}
		// check whether any input uses the same address as the output tx
		for j := range opts.Inputs {
			if opts.Inputs[j].Address == tx.Address {
				return nil, ErrSendingBackToInputs
			}
		}
	}
	*/
	// finalize bundle by adding the bundle hash
	finalizedBundle, err := bundle.Finalize(txs)
	if err != nil {
		return nil, err
	}

	signedFrags := []Trytes{}
	signedFrags, err = iccfpga.SignInputs(slot, opts.Inputs, finalizedBundle[0].Bundle)
	if err != nil {
		return nil, err
	}

	// add signed fragments to txs
	var indexFirstInputTx int
	for i := range txs {
		if txs[i].Value < 0 {
			indexFirstInputTx = i
			break
		}
	}

	txs = bundle.AddTrytes(txs, signedFrags, indexFirstInputTx)

	// finally return built up txs as raw trytes
	return transaction.MustFinalTransactionTrytes(txs), nil
}
