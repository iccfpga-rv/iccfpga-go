package main

import (
	"flag"
	"fmt"
	"time"

	"github.com/fatih/structs"
	. "github.com/iotaledger/iota.go/api"
	"gitlab.com/iccfpga-rv/iccfpga-go-lib/iccfpga"
)

// how many milestones back to start the random walk from
const depth = 3

var keyIndex int = 0

// has to be kept secret ... the weakness of everything :scream:
var apiKey = [...]byte{0xb2, 0x33, 0x12, 0x56, 0x41, 0xf0, 0xcc, 0x60, 0x9c, 0x6f, 0x36, 0x30, 0xe7, 0xf3, 0xb2, 0xfd, 0xb7, 0x2b, 0xbd, 0x2e,
	0x94, 0x40, 0xa5, 0xb0, 0x0a, 0xb5, 0x83, 0x65, 0x5b, 0x01, 0xb1, 0x43, 0xdf, 0x31, 0x3e, 0x9a, 0xa0, 0x90, 0x72, 0x02, 0xdf, 0x5f, 0x16,
	0x40, 0x8e, 0x64, 0xf4, 0xd4}

// example-struct
type JsonData struct {
	Id     string
	Name   string
	Email  string
	Phones []string
}

func main() {
	//	device := flag.String("device", "/dev/ttyS0", "serial device")
	device := flag.String("device", "/dev/ttyUSB0", "serial device")
	endpoint := flag.String("endpoint", "https://mainnet01.hornet.zone", "IOTA node to use")
	mwm := flag.Int("mwm", 14, "Min-Weight-Magnitude; Use 9 for devnet, 14 for mainnet")
	numTX := flag.Int("count", 10, "Number of bundles to generate")
	debug := flag.Bool("debug", false, "Debug output")

	flag.Parse()

	if *numTX > 100 {
		panic("too many bundles")
	}

	if *mwm != 9 && *mwm != 14 {
		panic("use mwm 9 or 14")
	}

	config := iccfpga.ICCFPGAConfig{
		Serial:        *device,
		ApiKey:        apiKey,
		KeepSeedInRAM: true,
		Verbose:       false,
		Debug:         *debug,
	}
	var iccfpga iccfpga.ICCFPGA

	err := iccfpga.Init(config)
	must(err)

	version, err := iccfpga.GetVersion()
	must(err)

	fmt.Printf("iccfpga version: %s\n\n", version)

	// create a new API instance
	api, err := ComposeAPI(HTTPClientSettings{
		URI: *endpoint,
		// (!) if no PoWFunc is supplied, then the connected node is requested to do PoW for us
		// via the AttachToTangle() API call.
		LocalProofOfWorkFunc: iccfpga.PoWFunc,
	})
	must(err)

	// some example data
	jsonData := JsonData{
		Id:     "08154711",
		Name:   "Some Name",
		Email:  "some.name@example.org",
		Phones: []string{"1234", "5678", "9123", "4567", "8912"},
	}

	address := "9RA9TIGOGWHOXCKSAW9KNMDUAGCIJNZVWRPPHYJBQTEVQWUDZ9MHDBAFHBRPWDSKBCRPNCTEA9FAECLMC"
	tag := "ICCFPGA9EXAMPLE"

	tips, err := api.GetTransactionsToApprove(3)

	timestamp := uint64(time.Now().UnixNano() / int64(time.Second))
	hash, trytes, err := iccfpga.ApiJsonDataTX(uint32(timestamp), address, tag, &tips.TrunkTransaction, &tips.BranchTransaction, uint32(*mwm), structs.Map(jsonData))
	must(err)

	// broadcast transactions
	_, err = api.BroadcastTransactions(trytes)
	must(err)

	fmt.Println("broadcasted bundle with tail tx hash: ", hash)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
